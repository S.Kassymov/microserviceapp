﻿using AutoMapper;
using Basket.API.Entities;
using Basket.API.Repositories.Interfaces;
using EventBusRabbitMQ.Common;
using EventBusRabbitMQ.Events;
using EventBusRabbitMQ.Producers;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace Basket.API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class BasketController : ControllerBase
    {
        private readonly IBasketCartRepository _repository;
        private readonly EventBusRabbitMQProducer _eventBus;
        private readonly IMapper _mapper;

        public BasketController(IBasketCartRepository repository, EventBusRabbitMQProducer eventBus, IMapper mapper)
        {
            _repository = repository;
            _eventBus = eventBus;
            _mapper = mapper;
        }

        [HttpGet]
        [ProducesResponseType(typeof(BasketCart),(int)HttpStatusCode.OK)]
        public async Task<ActionResult<BasketCart>> GetBasket(string name)
        {
            var basket = await _repository.GetBasketCart(name);
            return Ok(basket ?? new BasketCart(name));
        }

        [HttpPost]
        [ProducesResponseType(typeof(BasketCart),(int)HttpStatusCode.OK)]
        public async Task<ActionResult<BasketCart>> UpdateBasket(BasketCart basketCart)
        {
            var updatedBasket = await _repository.UpdateBasketCart(basketCart);
            return Ok(updatedBasket);
        }

       [HttpDelete]
       [ProducesResponseType(typeof(void), (int)HttpStatusCode.OK)]
       public async Task<IActionResult> DeleteBasket(string name)
        {
            return Ok(await _repository.DeleteBasketCart(name));
        }

        [Route("action")]
        [HttpPost]
        [ProducesResponseType(typeof(void), (int)HttpStatusCode.Created)]
        [ProducesResponseType(typeof(void), (int)HttpStatusCode.BadRequest)]
        public async Task<IActionResult> Checkout([FromBody] BasketCheckout basketCheckout)
        {
            var basket = await _repository.GetBasketCart(basketCheckout.Username);
            if(basket == null)
            {
                return BadRequest();
            }

            var removed = await _repository.DeleteBasketCart(basketCheckout.Username);
            if (!removed)
            {
                return BadRequest();
            }

            var eventMessage = _mapper.Map<BasketCheckoutEvent>(basketCheckout);
            eventMessage.RequestId = Guid.NewGuid();

            _eventBus.PublishBasketCheckout(EventBusConstants.BasketCheckoutQueue, eventMessage);

            return Accepted();
        }
    }
}
