﻿using Basket.API.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Basket.API.Repositories.Interfaces
{
    public interface IBasketCartRepository
    {
        Task<BasketCart> GetBasketCart(string name);
        Task<BasketCart> UpdateBasketCart(BasketCart basketCart);
        Task<bool> DeleteBasketCart(string name);
    }
}
