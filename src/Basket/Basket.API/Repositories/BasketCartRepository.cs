﻿using Basket.API.Data.Interfaces;
using Basket.API.Entities;
using Basket.API.Repositories.Interfaces;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Basket.API.Repositories
{
    public class BasketCartRepository : IBasketCartRepository
    {
        private readonly IBasketCartContext _context;

        public BasketCartRepository(IBasketCartContext context)
        {
            _context = context ?? throw new ArgumentNullException(nameof(context));
        }

        

        public async Task<BasketCart> GetBasketCart(string name)
        {
            var basket = await _context.Redis.StringGetAsync(name);

            if (basket.IsNullOrEmpty)
            {
                return null;
            }
            try
            {
                return JsonConvert.DeserializeObject<BasketCart>(basket);
            }
            catch(Exception)
            {
                return null;
            }
            
        }

        public async Task<BasketCart> UpdateBasketCart(BasketCart basketCart)
        {
            var updated = await _context.Redis.StringSetAsync(basketCart.Username, JsonConvert.SerializeObject(basketCart));
            if (!updated)
            {
                return null;
            }
            return await GetBasketCart(basketCart.Username);
        }

        public async Task<bool> DeleteBasketCart(string name)
        {
            return await _context.Redis.KeyDeleteAsync(name);
        }
    }
}
